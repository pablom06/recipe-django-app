from django import forms

class SignUpForm(forms.Form):
    username = forms.Charfield(max_length=150)
    first_name = forms.Charfield(max_length=150)
    last_name = forms.Charfield(max_length=150)
    password = forms.Charfield(
        max_length=150
        widget=forms.PasswordInput,
        )
    password_confirmation = forms.Charfield(
        max_length=150
        widget=forms.PasswordInput,
        )
