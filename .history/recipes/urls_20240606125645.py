from django.urls import path
from recipes.views import recipe_list, show_recipe, add_recipe, edit_recipe
from django.shortcuts import redirect

def redirect_to_recipes(request):
    return redirect('recipe_list')

urlpatterns = [
    path('recipes/', recipe_list, name='recipe_list'),
    path('recipes/add/', add_recipe, name='add_recipe'),
    path('recipes/<int:id>/', show_recipe, name='recipe_detail'),
    path('recipes/edit/<int:id>/', edit_recipe, name='edit_recipe'),
    path('', redirect_to_recipes, name=),
]
