from django.shortcuts import render, redirect
from django.contrib.auth import login
from django.contrib.auth.models import User
from accounts.forms import SignUpForm

def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            password = form.cleaned_data['password']
            password_confirmation = form.cleaned_data['password_confirmation']
            if password == password_confirmation:
                user = User.objects.create_user(
                    username=form.cleaned_data['username'],
                    first_name=form.cleaned_data['first_name'],
                    last_name=form.cleaned_data['last_name'],
                    password=password,
                )
                user.save()
                login(request, user)
                return redirect('recipe_list') 
            else:
                form.add_error('password_confirmation', 'Passwords do not match')
    else:
        form = SignUpForm()

    context = {
        'form': form,
    }
    return render(request, 'accounts/signup.html', context)
