from django.urls import path
from recipes.views import recipe_list, show_recipe
from django.urls import path
from recipes.views import recipe_list, show_recipe

urlpatterns = [
    path('', recipe_list, name='recipe_list'),
    path('recipe/<int:id>/', show_recipe, name='recipe_detail'),
]

