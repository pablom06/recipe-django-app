from django.db import models

class Recipe(models.Model):
    title = models.CharField(max_length=200)
    picture = models.URL()
    time_required = models.IntegerField()
    instructions = models.TextField()
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title
