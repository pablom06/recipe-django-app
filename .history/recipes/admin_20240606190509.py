from django.contrib import admin
from recipes.models import Recipe

@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_display = [
        "title",
        "id",
        "created_on",
    ]
    class RecipeStepInline(admin.TabularInline):
    model = RecipeStep
    extra = 1

class RecipeIngredientInline(admin.TabularInline):
    model = RecipeIngredient
    extra = 1
