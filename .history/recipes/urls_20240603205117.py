from django.urls import path
from recipes.views import recipe_list, show_recipe

urlpatterns = [
    path()
    path('recipes/<int:id>/', show_recipe),
]
