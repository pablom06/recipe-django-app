from django.contrib import admin
from recipes.models import Recipe

@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_display = ('title', 'created_on', 'rating')
    list_filter = ('created_on', 'rating')
    search_fields = ('title', 'description')
