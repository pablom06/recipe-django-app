from django import forms

class SignUpForm(forms.Form):
    username = forms.Charfield(max_length=150)
    first_name = forms.Charfield(max_length=150)
    last_name = forms.Charfield(max_length=150)
    password = 
