from django.shortcuts import render, get
from recipes.models import Recipe

def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    return render(request, 'recipes/detail.html')
