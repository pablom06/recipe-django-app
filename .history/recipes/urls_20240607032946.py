from django.urls import path
from recipes.views import recipe_list, show_recipe, add_recipe, edit_recipe, share_recipe, rate_recipe, add_recipe_ingredient, add_recipe_step
from django.shortcuts import redirect
from . import views

def redirect_to_recipes(request):
    return redirect('recipe_list')

urlpatterns = [
    path('', views.recipe_list, name='recipe_list'),
    path('recipes/add/', add_recipe, name='add_recipe'),
    path('recipes/<int:id>/', show_recipe, name='recipe_detail'),
    path('recipes/edit/<int:id>/', edit_recipe, name='edit_recipe'),
    path('', redirect_to_recipes, name="home"),
    path('recipe/<int:recipe_id>/add_step/', aadd_recipe_step, name='add_recipe_step'),
    path('recipe/<int:recipe_id>/add_ingredient/', views.add_recipe_ingredient, name='add_recipe_ingredient'),
    path('recipes/<int:id>/share/', share_recipe, name='share_recipe'),
    path('recipes/<int:recipe_id>/rate/', views.rate_recipe, name='rate_recipe'),
]
