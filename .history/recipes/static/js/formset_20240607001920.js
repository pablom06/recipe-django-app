function autoResizeTextArea(textarea) {
    textarea.style.height = 'auto';
    textarea.style.height = (textarea.scrollHeight) + 'px';
}

document.querySelectorAll('textarea').forEach(function(textarea) {
    textarea.addEventListener('input', function() {
        autoResizeTextArea(textarea);
    });
    autoResizeTextArea(textarea); // Initial call to adjust size based on initial content
});

function cloneFormset(prefix, formCount, formContainerSelector) {
    var formClone = document.querySelector(`${formContainerSelector}:last-of-type`).cloneNode(true);
    formClone.querySelectorAll('input, select, textarea').forEach(function(input) {
        input.name = input.name.replace(`-${formCount - 1}-`, `-${formCount}-`);
        input.id = input.id.replace(`_${formCount - 1}_`, `_${formCount}_`);
        input.value = '';
        if (input.tagName.toLowerCase() === 'textarea') {
            input.style.height = 'auto'; // Reset height
            input.addEventListener('input', function() {
                autoResizeTextArea(input);
            });
        }
    });
    formClone.querySelectorAll('.remove-ingredient, .remove-step').forEach(function(button) {
        button.addEventListener('click', function() {
            this.closest(formContainerSelector).remove();
        });
    });
    return formClone;
}

document.getElementById('add-ingredient').addEventListener('click', function() {
    var totalForms = document.getElementById('id_ingredients-TOTAL_FORMS');
    var currentCount = parseInt(totalForms.value);
    var formClone = cloneFormset('ingredients', currentCount, '.ingredients-formset-item');
    totalForms.value = currentCount + 1;
    document.getElementById('ingredients-formset').appendChild(formClone);
});

document.getElementById('add-step').addEventListener('click', function() {
    var totalForms = document.getElementById('id_steps-TOTAL_FORMS');
    var currentCount = parseInt(totalForms.value);
    var formClone = cloneFormset('steps', currentCount, '.steps-formset-item');
    totalForms.value = currentCount + 1;
    document.getElementById('steps-formset').appendChild(formClone);
});

document.querySelectorAll('.remove-ingredient').forEach(function(button) {
    button.addEventListener('click', function() {
        this.closest('.ingredients-formset-item').remove();
    });
});

document.querySelectorAll('.remove-step').forEach(function(button) {
    button.addEventListener('click', function() {
        this.closest('.steps-formset-item').remove();
    });
});

