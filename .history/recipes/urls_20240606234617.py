from django.urls import path
{% extends 'recipes/base.html' %}
{% load static %}

{% block title %}Recipe Details{% endblock %}

{% block content %}
    <section id="recipe-details" class="detail-page">
        <h2>{{ recipe_object.title }}</h2>
        <img src="{{ recipe_object.picture }}" alt="{{ recipe_object.title }}">
        <div>{{ recipe_object.rating }} stars</div>
        <p><strong>Description:</strong> {{ recipe_object.description }}</p>
        <p><strong>Created on:</strong> {{ recipe_object.created_on|date:"d/m/Y" }}</p>
        <p><strong>Ingredients:</strong></p>
        <p>{{ recipe_object.ingredients }}</p>
        <p><strong>Steps:</strong></p>
        <p>{{ recipe_object.steps }}</p>
        <p><strong>Prep Time:</strong> {{ recipe_object.prep_time }} minutes</p>
        <p><strong>Cook Time:</strong> {{ recipe_object.cook_time }} minutes</p>
        <p><strong>Serves:</strong> {{ recipe_object.serves }}</p>
        <a href="{% url 'edit_recipe' recipe_object.id %}" class="edit-recipe-button">Edit</a>

        <!-- Rating Form -->
        <h2>Rate this Recipe</h2>
        <form method="post" action="{% url 'rate_recipe' recipe_object.id %}">
            {% csrf_token %}
            {{ rating_form.as_p }}
            <button type="submit">Submit Rating</button>
        </form>
    </section>
{% endblock %}
from django.shortcuts import redirect
from . import views

def redirect_to_recipes(request):
    return redirect('recipe_list')

urlpatterns = [
    path('', views.recipe_list, name='recipe_list'),
    path('recipes/add/', add_recipe, name='add_recipe'),
    path('recipes/<int:id>/', show_recipe, name='recipe_detail'),
    path('recipes/edit/<int:id>/', edit_recipe, name='edit_recipe'),
    path('', redirect_to_recipes, name="home"),
    path('recipe/<int:recipe_id>/add_step/', views.add_recipe_step, name='add_recipe_step'),
    path('recipe/<int:recipe_id>/add_ingredient/', views.add_recipe_ingredient, name='add_recipe_ingredient'),
    path('recipes/<int:id>/share/', share_recipe, name='share_recipe'),
]
