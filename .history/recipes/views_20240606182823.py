from django.shortcuts import render, get_object_or_404, redirect
from django.db.models import Q
from django.forms import modelformset_factory
from .models import Recipe, RecipeStep, RecipeIngredient
from .forms import RecipeForm, RecipeStepForm, RecipeIngredientForm

def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    context = {
        'recipe_object': recipe,
    }
    return render(request, 'recipes/detail.html', context)

def recipe_list(request):
    query = request.GET.get('q')
    if query:
        recipes = Recipe.objects.filter(
           Q(title__icontains=query) |
           Q(description__icontains=query) |
           Q(recipe_ingredients__name__icontains=query) |
           Q(recipe_ingredients__quantity__icontains=query) |
           Q(recipe_steps__instruction__icontains=query)
        ).distinct()
    else:
        recipes = Recipe.objects.all()

    context = {
        "recipe_list": recipes,
        "query": query,
    }
    return render(request, "recipes/list.html", context)

def add_recipe(request):
    RecipeStepFormSet = modelformset_factory(RecipeStep, form=RecipeStepForm, extra=1, can_delete=True)
    RecipeIngredientFormSet = modelformset_factory(RecipeIngredient, form=RecipeIngredientForm, extra=1, can_delete=True)

    if request.method == 'POST':
        form = RecipeForm(request.POST)
        steps_formset = RecipeStepFormSet(request.POST, queryset=RecipeStep.objects.none(), prefix='steps')
        ingredients_formset = RecipeIngredientFormSet(request.POST, queryset=RecipeIngredient.objects.none(), prefix='ingredients')

        if form.is_valid() and steps_formset.is_valid() and ingredients_formset.is_valid():
            recipe = form.save()
            for step_form in steps_formset:
                if step_form.cleaned_data and not step_form.cleaned_data.get('DELETE'):
                    step = step_form.save(commit=False)
                    step.recipe = recipe
                    step.save()
            for ingredient_form in ingredients_formset:
                if ingredient_form.cleaned_data and not ingredient_form.cleaned_data.get('DELETE'):
                    ingredient = ingredient_form.save(commit=False)
                    ingredient.recipe = recipe
                    ingredient.save()
            return redirect('recipe_list')
    else:
        form = RecipeForm()
        steps_formset = RecipeStepFormSet(queryset=RecipeStep.objects.none(), prefix='steps')
        ingredients_formset = RecipeIngredientFormSet(queryset=RecipeIngredient.objects.none(), prefix='ingredients')

    return render(request, 'recipes/add_recipe.html', {
        'form': form,
        'steps_formset': steps_formset,
        'ingredients_formset': ingredients_formset,
    })

def edit_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    RecipeStepFormSet = modelformset_factory(RecipeStep, form=RecipeStepForm, extra=1, can_delete=True)
    RecipeIngredientFormSet = modelformset_factory(RecipeIngredient, form=RecipeIngredientForm, extra=1, can_delete=True)

    if request.method == 'POST':
        form = RecipeForm(request.POST, instance=recipe)
        steps_formset = RecipeStepFormSet(request.POST, queryset=RecipeStep.objects.filter(recipe=recipe), prefix='steps')
        ingredients_formset = RecipeIngredientFormSet(request.POST, queryset=RecipeIngredient.objects.filter(recipe=recipe), prefix='ingredients')

        if form.is_valid() and steps_formset.is_valid() and ingredients_formset.is_valid():
            form.save()
            for step_form in steps_formset:
                if step_form.cleaned_data:
                    if step_form.cleaned_data.get('DELETE'):
                        step_form.instance.delete()
                    else:
                        step = step_form.save(commit=False)
                        step.recipe = recipe
                        step.save()
            for ingredient_form in ingredients_formset:
                if ingredient_form.cleaned_data:
                    if ingredient_form.cleaned_data.get('DELETE'):
                        ingredient_form.instance.delete()
                    else:
                        ingredient = ingredient_form.save(commit=False)
                        ingredient.recipe = recipe
                        ingredient.save()
            return redirect('recipe_detail', id=recipe.id)
    else:
        form = RecipeForm(instance=recipe)
        steps_formset = RecipeStepFormSet(queryset=RecipeStep.objects.filter(recipe=recipe), prefix='steps')
        ingredients_formset = RecipeIngredientFormSet(queryset=RecipeIngredient.objects.filter(recipe=recipe), prefix='ingredients')

    return render(request, 'recipes/edit_recipe.html', {
        'form': form,
        'recipe': recipe,
        'steps_formset': steps_formset,
        'ingredients_formset': ingredients_formset,
    })

