from django.db import models

class Recipe(models.Model):
    title = models.CharField(max_length=200)
    picture = models.URLField()
    description = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
 rating = models.DecimalField(max_digits=5, decimal_places=2)
    ingredients = models.TextField()
    steps = models.TextField()
    prep_time = models.IntegerField()
    cook_time = models.IntegerField()
    serves = models.IntegerField()
