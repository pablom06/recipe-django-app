from django import forms

class SignUpForm(forms.Form):
    username = forms.Charfield()
