from django.db import migrations, models

def set_default_values(apps, schema_editor):
    Recipe = apps.get_model('recipes', 'Recipe')
    Recipe.objects.filter(rating__isnull=True).update(rating=0.00)
    Recipe.objects.filter(serves__isnull=True).update(serves=1)

class Migration(migrations.Migration):

    dependencies = [
        ('recipes', 'previous_migration_file'),
    ]

    operations = [
        migrations.AlterField(
            model_name='recipe',
            name='rating',
            field=models.DecimalField(default=0.00, max_digits=3, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='recipe',
            name='serves',
            field=models.IntegerField(default=1),
        ),
        migrations.RunPython(set_default_values),
    ]
