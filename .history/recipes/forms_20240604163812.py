from django.forms import ModelForm
from recipes.models import Recipe

class RecipeForm(ModelForm):
    class Meta:
        model = Recipe
        fields = ["picture",
                  "description",
                  "created on",

                  ]

    rating = models.DecimalField(max_digits=3, decimal_places=2, null=False, default=0.00)
    ingredients = models.TextField()
    steps = models.TextField()
    prep_time = models.DecimalField(max_digits=5, decimal_places=2, default=0.00)
    cook_time = models.DecimalField(max_digits=5, decimal_places=2, default=0.00)
    serves = models.IntegerField(null=False, default=1)
