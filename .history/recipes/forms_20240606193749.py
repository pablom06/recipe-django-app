from django.forms import ModelForm
from .models import Recipe, RecipeStep, RecipeIngredient

class RecipeForm(ModelForm):
    class Meta:
        model = Recipe
        exclude = ['created_on']

class RecipeStepForm(ModelForm):
    class Meta:
        model = RecipeStep
        fields = ['step_number', 'instruction']

class RecipeIngredientForm(ModelForm):
    class Meta:
        model = RecipeIngredient
        fields = ['name', 'quantity', 'instruction']
            'instruction': forms.Textarea(attrs={'rows': 2, 'cols': 40})  # Adjust size here
