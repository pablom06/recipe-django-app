from django.urls import path
from recipes.views import recipeshow_recipe

urlpatterns = [
    path('recipes/<int:id>/', show_recipe),
]
