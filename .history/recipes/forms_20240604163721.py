from django.forms import ModelForm
from recipes.models import Recipe

class RecipeForm(ModelForm):
    class Meta:
        model = Recipe
        fields = ["title", "description", "instructions", "time_required", "instructions", "image_url"]

title = models.CharField(max_length=200)
    picture = models.URLField()
    description = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
    rating = models.DecimalField(max_digits=3, decimal_places=2, null=False, default=0.00)
    ingredients = models.TextField()
    steps = models.TextField()
    prep_time = models.DecimalField(max_digits=5, decimal_places=2, default=0.00)
    cook_time = models.DecimalField(max_digits=5, decimal_places=2, default=0.00)
    serves = models.IntegerField(null=False, default=1)
