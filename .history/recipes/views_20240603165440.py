from django.shortcuts import render
from re

def show_recipe(request, id):
    return render(request, 'recipes/detail.html')
