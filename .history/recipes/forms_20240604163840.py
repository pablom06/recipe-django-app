from django.forms import ModelForm
from recipes.models import Recipe

class RecipeForm(ModelForm):
    class Meta:
        model = Recipe
        fields = ["picture",
                  "description",
                  "created on",
                    "rating",
                    "ingredients",
                    "steps",
                    "prep_time",
                    "cook_time",
                    "serves",
                    "title",

                  ]
    