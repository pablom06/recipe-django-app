from django.db import models

class Recipe(models.Model):
    title = models.CharField(max_length=200)
    picture = models.URLField()
    description = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
    rating = models.DecimalField(max_digits=3, decimal_places=2, null=False, default=0.00)
    ingredients = models.TextField()
    steps = models.TextField()
    prep_time = models.DecimalField(max_digits=5, decimal_places=2, default=0.00)
    cook_time = models.DecimalField(max_digits=5, decimal_places=2, default=0.00)
    serves = models.IntegerField(null=False, default=1)

    def __str__(self):
        return self.title

class RecipeStep(models.Model):
    step_number = models.PositiveSmallIntegerField()
    instructions = models.TextField()
    recipe = models.ForeignKey(Recipe, related_name='steps', on_delete=models.CASCADE)

    class Meta:
        ordering = ['step_number']

class RecipeIngredient(models.Model):
    name = models.CharField(max_length=100)
    quantity = models.CharField(max_length=50)
    recipe = models.ForeignKey(Recipe, related_name='ingredients', on_delete=models.CASCADE)

    def __str__(self):
        return self.name
