from django.shortcuts import render
from recipes.models import Recipe

def show_recipe(request, id):
    return render(request, 'recipes/detail.html')
