from django.urls import path
from recipes.views import recipe_list, show_recipe
from . import views

urlpatterns = [
    path("", views.recipe_list_view, name="recipe_list"),
    path('recipes/<int:id>/', show_recipe),
]
