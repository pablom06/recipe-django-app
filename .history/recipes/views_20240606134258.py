from django.shortcuts import render, get_object_or_404, redirect
from django.db.models import Q
from recipes.models import Recipe
from .forms import RecipeForm

def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    context = {
        'recipe_object': recipe,
    }
    return render(request, 'recipes/detail.html', context)

def recipe_list(request):
    query = request.GET.get('q')
    if query:
        recipes = Recipe.objects.filter(
            Q(title__icontains=query) | Q(description__icontains=query) | Q(ingredients__icontains=query)
        )
    else:
        recipes = Recipe.objects.all()

    context = {
        "recipe_list": recipes,
        "query": query,
    }
    return render(request, "recipes/list.html", context)

def add_recipe(request):
    if request.method == 'POST':
        form = RecipeForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('recipe_list')
    else:
        form = RecipeForm()
    return render(request, 'recipes/add_recipe.html', {'form': form})

def edit_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    if request.method == 'POST':
        form = RecipeForm(request.POST, instance=recipe)
        if form.is_valid():
            form.save()
            return redirect('recipe_detail', id=recipe.id)
    else:
        form = RecipeForm(instance=recipe)
    return render(request, 'recipes/edit_recipe.html', {'form': form, 'recipe': recipe})

from django.shortcuts import render, redirect
from django.contrib.auth import login
from django.contrib.auth.models import User
from accounts.forms import SignUpForm


    return render(request, 'accounts/signup.html', context)
