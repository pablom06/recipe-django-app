from django.shortcuts import render, get_object_or_404, redirect
from .models import Recipe, RecipeStep, RecipeIngredient
from .forms import RecipeForm, RecipeStepForm, RecipeIngredientForm
from django.db.models import Q
from django.forms import modelformset_factory
from django.contrib.auth.decorators import login_required

@login_required
def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    context = {
        'recipe_object': recipe,
    }
    return render(request, 'recipes/detail.html', context)

def recipe_list(request):
    query = request.GET.get('q')
    if query:
        recipes = Recipe.objects.filter(
           Q(title__icontains=query) |
            Q(description__icontains=query) |
            Q(recipe_ingredients__name__icontains=query) |
            Q(recipe_ingredients__quantity__icontains=query) |
            Q(recipe_steps__instruction__icontains=query)
        ).distinct()
    else:
        recipes = Recipe.objects.all()

    context = {
        "recipe_list": recipes,
        "query": query,
    }
    return render(request, "recipes/list.html", context)

@login_required
def add_recipe(request):
    RecipeStepFormSet = modelformset_factory(RecipeStep, form=RecipeStepForm, extra=1)
    RecipeIngredientFormSet = modelformset_factory(RecipeIngredient, form=RecipeIngredientForm, extra=1)

    if request.method == 'POST':
        form = RecipeForm(request.POST)
        steps_formset = RecipeStepFormSet(request.POST, queryset=RecipeStep.objects.none())
        ingredients_formset = RecipeIngredientFormSet(request.POST, queryset=RecipeIngredient.objects.none())

        if form.is_valid() and steps_formset.is_valid() and ingredients_formset.is_valid():
            recipe = form.save()
            for step_form in steps_formset:
                step = step_form.save(commit=False)
                step.recipe = recipe
                step.save()
            for ingredient_form in ingredients_formset:
                ingredient = ingredient_form.save(commit=False)
                ingredient.recipe = recipe
                ingredient.save()
            return redirect('recipe_list')
    else:
        form = RecipeForm()
        steps_formset = RecipeStepFormSet(queryset=RecipeStep.objects.none())
        ingredients_formset = RecipeIngredientFormSet(queryset=RecipeIngredient.objects.none())

    return render(request, 'recipes/add_recipe.html', {
        'form': form,
        'steps_formset': steps_formset,
        'ingredients_formset': ingredients_formset,
    })

@login_required
def edit_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    RecipeStepFormSet = modelformset_factory(RecipeStep, form=RecipeStepForm, extra=1, can_delete=True)
    RecipeIngredientFormSet = modelformset_factory(RecipeIngredient, form=RecipeIngredientForm, extra=1, can_delete=True)

    if request.method == 'POST':
        form = RecipeForm(request.POST, instance=recipe)
        steps_formset = RecipeStepFormSet(request.POST, queryset=RecipeStep.objects.filter(recipe=recipe), prefix='steps')
        ingredients_formset = RecipeIngredientFormSet(request.POST, queryset=RecipeIngredient.objects.filter(recipe=recipe), prefix='ingredients')

        if form.is_valid() and steps_formset.is_valid() and ingredients_formset.is_valid():
            form.save()
            for step_form in steps_formset:
                if step_form.cleaned_data:
                    if step_form.cleaned_data.get('DELETE'):
                        step_form.instance.delete()
                    else:
                        step = step_form.save(commit=False)
                        step.recipe = recipe
                        step.save()
            for ingredient_form in ingredients_formset:
                if ingredient_form.cleaned_data:
                    if ingredient_form.cleaned_data.get('DELETE'):
                        ingredient_form.instance.delete()
                    else:
                        ingredient = ingredient_form.save(commit=False)
                        ingredient.recipe = recipe
                        ingredient.save()
            return redirect('recipe_detail', id=recipe.id)
    else:
        form = RecipeForm(instance=recipe)
        steps_formset = RecipeStepFormSet(queryset=RecipeStep.objects.filter(recipe=recipe), prefix='steps')
        ingredients_formset = RecipeIngredientFormSet(queryset=RecipeIngredient.objects.filter(recipe=recipe), prefix='ingredients')

    return render(request, 'recipes/edit_recipe.html', {
        'form': form,
        'recipe': recipe,
        'steps_formset': steps_formset,
        'ingredients_formset': ingredients_formset,
    })

@login_required
def add_recipe_step(request, recipe_id):
    recipe = get_object_or_404(Recipe, id=recipe_id)
    if request.method == "POST":
        step_number = request.POST.get('step_number')
        instruction = request.POST.get('instruction')
        if step_number and instruction:
            RecipeStep.objects.create(recipe=recipe, step_number=step_number, instruction=instruction)
        return redirect('recipe_detail', id=recipe.id)  # Ensure this redirects to 'recipe_detail'
    return render(request, 'recipes/detail.html', {'recipe': recipe})

@login_required
def add_recipe_ingredient(request, recipe_id):
    recipe = get_object_or_404(Recipe, id=recipe_id)
    if request.method == "POST":
        name = request.POST.get('name')
        quantity = request.POST.get('quantity')
        if name and quantity:
            RecipeIngredient.objects.create(recipe=recipe, name=name, quantity=quantity)
        return redirect('recipe_detail', id=recipe.id)  # Ensure this redirects to 'recipe_detail'
    return render(request, 'recipes/detail.html', {'recipe': recipe})
