from django.db import models

class Recipe(models.Model):
    title = models.CharField(max_length=200)
    picture = models.URLField()
    descr

    def __str__(self):
        return self.title
