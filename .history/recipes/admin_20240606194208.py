from django.contrib import admin
from recipes.models import Recipe, RecipeStep, RecipeIngredient

@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_display = [
        "title",
        "id",
        "created_on",
    ]

class RecipeStepInline(admin.TabularInline):
    model = RecipeStep
    extra = 1

class RecipeIngredientInline(admin.TabularInline):
    model = RecipeIngredient
    extra = 1
    fields = ['name', 'quantity', 'instruction']

@admin.register(RecipeStep)
class RecipeStepAdmin(admin.ModelAdmin):
    inlines = [Recipe]

@admin.register(RecipeIngredient)
class RecipeIngredientAdmin(admin.ModelAdmin):
    list_display = ["recipe", "name", "quantity", "instruction"]
