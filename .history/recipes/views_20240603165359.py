from django.shortcuts import render

def show_recipe(request, id):
    return render(request, 'recipes/detail.html')
