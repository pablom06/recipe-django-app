from django.forms import ModelForm
from .models import Recipe, RecipeStep, RecipeIngredient, Rating 
from django import forms

class RecipeForm(ModelForm):
    class Meta:
        model = Recipe
        exclude = ['created_on']
        widgets = {
            'description': forms.Textarea(attrs={'rows':3, 'cols':40})
        }

class RecipeStepForm(ModelForm):
    class Meta:
        model = RecipeStep
        fields = ['step_number', 'instruction']
        widgets = {
            'instruction': forms.Textarea(attrs={'rows':3, 'cols':40})
        }

class RecipeIngredientForm(ModelForm):
    class Meta:
        model = RecipeIngredient
        fields = ['name', 'quantity', 'measure','instruction']
        widgets = {
            'instruction': forms.Textarea(attrs={'rows':3, 'cols':40})
        }
