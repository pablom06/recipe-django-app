from django.forms import ModelForm
from recipes.models import Recipe
from django import forms

class RecipeForm(ModelForm):
    class Meta:
        model = Recipe
        exclude = ['created_on']

class LoginForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput
    )
