from django.shortcuts import render, redirect
from django.contrib.auth import login
from django.contrib.auth.models import User
from accounts.forms import SignUpForm
from django.db import IntegrityError
from django.contrib.auth import authenticate, login

def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            if User.objects.filter(username=username).exists():
                form.add_error('username', 'Username already exists')
            else:
                password = form.cleaned_data['password']
                password_confirmation = form.cleaned_data['password_confirmation']
                if password != password_confirmation:
                    form.add_error('password_confirmation', 'Passwords do not match')
                else:
                    try:
                        user = User.objects.create_user(
                            username=form.cleaned_data['username'],
                            first_name=form.cleaned_data['first_name'],
                            last_name=form.cleaned_data['last_name'],
                            password=form.cleaned_data['password']
                        )
                        login(request, user)
                        return redirect('recipe_list')
                    except IntegrityError:
                        form.add_error(None, 'An error occurred while creating the user.')
    else:
        form = SignUpForm()
    return render(request, 'accounts/signup.html', {'form': form})

def user_login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            user = authenticate(
                request,
                username=username, password=password)
