from django.shortcuts import render, redirect
from django.contrib.auth import login
from django.contrib.auth.forms import User
from accounts.forms import SignUpForm

def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = User.objects.create_user(
                username=form.cleaned_data['username'],
                first_name=form.cleaned_data['first_name'],
                last_name=form.cleaned_data['last_name'],
                password=form.cleaned_data['password'],
                password_confirmation=form.cleaned_data['password_confirmation'],
            )
            login(request, userna
