from django import forms

class SignUpForm(forms.Form):
    username = forms.Charfield(max_length=150)
    
