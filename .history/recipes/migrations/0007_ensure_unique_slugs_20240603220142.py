from django.db import migrations
from django.utils.text import slugify

def ensure_unique_slugs(apps, schema_editor):
    Recipe = apps.get_model('recipes', 'Recipe')
    recipes = Recipe.objects.all()
    for recipe in recipes:
        if not recipe.slug:
            recipe.slug = slugify(recipe.title)
            original_slug = recipe.slug
            unique_slug = original_slug
            counter = 1
            while Recipe.objects.filter(slug=unique_slug).exists():
                unique_slug = f"{original_slug}-{counter}"
                counter += 1
            recipe.slug = unique_slug
            recipe.save()

class Migration(migrations.Migration):

    dependencies = [
        ('recipes', 'current_migration_dependency'),  # replace with the actual dependency
    ]

    operations = [
        migrations.RunPython(ensure_unique_slugs),
    ]
