from django.urls import path
from recipes.views import recipe_list, show_recipe
from . import views

urlpatterns = [
    path('', views.recipe_list, name='recipe_list'),
    path('recipe/<slug:slug>/', views.show_recipe, name='recipe_detail'),
]
