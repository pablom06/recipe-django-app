from django.db import models
from django.urls import reverse


class Recipe(models.Model):
    title = models.CharField(max_length=200)
    picture = models.URLField()
    description = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
    rating = models.DecimalField(max_digits=3, decimal_places=2, null=False, default=0.00)
    prep_time = models.DecimalField(max_digits=5, decimal_places=2, default=0.00)
    cook_time = models.DecimalField(max_digits=5, decimal_places=2, default=0.00)
    serves = models.IntegerField(null=False, default=1)

    def __str__(self):
        return self.title

    def update_rating(self):
        total_ratings = self.ratings.count()
        if total_ratings > 0:
            self.rating = sum(rating.value for rating in self.ratings.all()) / total_ratings
            self.save()

class Rating(models.Model):
    recipe = models.ForeignKey(Recipe, related_name='ratings', on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    value = models.PositiveSmallIntegerField()

    class Meta:
        unique_together = ('recipe', 'user')
    def get_absolute_url(self):
        return reverse('recipe_detail', args=[self.id])

class RecipeStep(models.Model):
    step_number = models.PositiveSmallIntegerField()
    instruction = models.TextField()
    recipe = models.ForeignKey(Recipe, related_name='steps', on_delete=models.CASCADE)

    class Meta:
        ordering = ['step_number']

class RecipeIngredient(models.Model):
    MEASURE_CHOICES = [
        ('oz', 'Ounces'),
        ('qt', 'Quart'),
        ('pt', 'Pint'),
        ('gal', 'Gallon'),
        ('tsp', 'Teaspoon'),
        ('tbsp', 'Tablespoon'),
        ('cup', 'Cup'),
        ('sprinkle', 'Sprinkle'),
        ('smidge', 'Smidge'),
        ('cloves', 'Cloves'),
        ('bushel', 'Bushel'),
        ('bunch', 'Bunch'),
        ('whole', 'Whole'),
    ]
    name = models.CharField(max_length=200)
    quantity = models.CharField(max_length=50)
    measure = models.CharField(max_length=20, choices=MEASURE_CHOICES, default='oz')
    instruction = models.TextField(null=True, blank=True)
    recipe = models.ForeignKey(Recipe, related_name='ingredients', on_delete=models.CASCADE)

    def __str__(self):
        return self.name
