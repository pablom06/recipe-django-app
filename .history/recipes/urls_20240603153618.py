from django.urls import path
from recipes.views import show_recipe

urlpatterns = [
    path('recipe/', show_recipe),
]
